import logging
from typing import List, Union

from telegram import Update, MessageEntity, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.constants import MessageEntityType, ParseMode
from telegram.ext import filters, ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, CallbackQueryHandler

import config

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)


def build_menu(
        buttons: List[InlineKeyboardButton],
        n_cols: int,
        header_buttons: Union[InlineKeyboardButton, List[InlineKeyboardButton]] = None,
        footer_buttons: Union[InlineKeyboardButton, List[InlineKeyboardButton]] = None
) -> List[List[InlineKeyboardButton]]:
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons if isinstance(header_buttons, list) else [header_buttons])
    if footer_buttons:
        menu.append(footer_buttons if isinstance(footer_buttons, list) else [footer_buttons])
    return menu


async def parse_entities(entities: tuple[MessageEntity], message):
    title = ''
    link = ''

    for entity in entities:
        if entity.type == MessageEntityType.TEXT_LINK:
            link = entity.url
        elif entity.type == MessageEntityType.BOLD and title == '':
            title = message[entity.offset:(entity.offset + entity.length)]

    title = title.strip()

    return title, link


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text="I'm a bot, please talk to me! @an0n_bl0ck")


async def send_button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query

    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    await query.answer()

    # get type of query event and origin message id
    data_type, origin_id = query.data.split(';')

    # if approve callback data
    if data_type == 'approve':
        # Forward message to main channel
        await query.copy_message(
            chat_id=config.MAIN_CHAN,
            parse_mode=ParseMode.HTML,
            disable_notification=True
        )

    # delete post message after
    await query.delete_message()


async def post(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message_id = update.effective_message.id
    entities = update.channel_post.entities

    title, link = await parse_entities(entities, update.effective_message.text)

    values = [title, link]
    # if any value is invalid
    if any(v in [None, ''] for v in values):
        return

    message = (f'#autopost #feed\n\n'
               f'<b>{title}</b>\n\n'
               f'{link}\n\n'
               f'〰 @an0n_bl0ck')

    # inline buttons
    button_list = [
        # each button has the event_type;origin_message_id
        InlineKeyboardButton("Approve", callback_data=f'approve;{message_id}'),
        InlineKeyboardButton("Reject", callback_data=f'reject;{message_id}'),
    ]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))

    # send formatted post message for approval
    await context.bot.send_message(
        chat_id=config.QUEUE_CHAN,
        text=message,
        parse_mode=ParseMode.HTML,
        disable_notification=True,
        reply_markup=reply_markup
    )


if __name__ == '__main__':
    application = ApplicationBuilder().token(config.TOKEN).build()

    start_handler = CommandHandler('start', start)
    application.add_handler(start_handler)

    # Message handler
    message_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), post)
    application.add_handler(message_handler)

    # Query callback handler
    application.add_handler(CallbackQueryHandler(send_button))

    application.run_polling()
